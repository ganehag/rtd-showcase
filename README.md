# rtd-showcase
Resistance thermometer are sensors used to measure temperature by 
correlating the resistance of the RTD element with temperature. 
This software implements a large amount of the various profiles 
needed to perform computation on these sensors.

# Usage


## Case 1
1. Select sensor from the dropdown
2. Input either temperature (in Celsius) or resistance (in Ohm), resolve the other value.

## Case 2
1. Switch the dropdown to "Resolve RTD" to use temperature and resistance to resolve an appropriate sensor.
